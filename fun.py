#!/usr/bin/python

def ChangeInt(a):
    a = 10

b = 2
ChangeInt(b)
print("b的结果是:",b)

def ChangeMe(mylist,str):
    mylist.append([1,2,3,4])
    mylist.append(101)
    print("函数内的值:",mylist)
    print("str:",str)
    return

mylist = [10,20,30]
ChangeMe(str= "good name",mylist=mylist)
print("函数外的值:",mylist)

def DefaultValue(name,age=35):
    print("age:",age)
    print("name:",name)

DefaultValue("baolin")

def UnkownParams(name,*vartuple):
    print("--------------")
    print("name:",name)
    for var in vartuple:
        print(var)
    return
UnkownParams("lillian")
UnkownParams("lillian2","lillian3","lillian4")

sum = lambda arg1, arg2:arg1+arg2;
div = lambda arg1, arg2:arg1/arg2

print(div(10,20))
print(sum(10,20))

def sum(arg1, arg2):
    print("arg1:",arg1,"arg2:",arg2)
    return arg1+arg2

print(sum(20,30))

import support
#import support
support.print_func("marry")