
list = ['abcd',786,2.23,'runoob',70.6,["good","girl"]]
tinylist = [123,'runoob']
print(list)
print(list[-1])
print(list[1:3])
print(list[2:])
print(list[2:]*2)

print(list+tinylist)

list = [1,1,2,3,3,4,4,5,6]
list2 = ["a","b"]
list.append(10)
print(list)
list.extend(list2)
print(list)
list.append(list2)
print(list)
list.insert(3,100)
print(list)
if 1 in list:
    list.remove(1)
print(list)
list.pop(2)
print(list)
print("index:",list.index(1))
print("count:",list.count(1))
list3 = [1,2,5,3,4,7,6]
list3.sort()
print(list3)
list3.reverse()
print(list3)
list4 = list3.copy()
list4.append(10)
print(list4)
print(list3)
#list.sort()
#print(list)

list.clear()
print(list)


