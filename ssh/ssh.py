import paramiko
import sys
"""
前提安装paramiko
pip install paramiko
用法：python ssh.py 源文件 目标文件
如: python ssh.py src.txt /home/dst.txt
"""

transport = paramiko.Transport("127.0.0.1", 22)
transport.connect(username='nicle', password="admin")
sftp = paramiko.SFTPClient.from_transport(transport)
print("上传文件{}到{}".format(sys.argv[1], sys.argv[2]))
sftp.put(sys.argv[1], sys.argv[2])
transport.close()
