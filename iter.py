#!/usr/bin/python

list = [1,2,3,4]
it = iter(list)

for x in it:
    print(x,end=' ')
'''
while True:
    try:
        print(next(it))
        print("-----")
    except StopIteration:
        print("####-----")
        exit(10)
'''
class MyNumbers:
    def __iter__(self):
        self.a = 1
        return self

    def __next__(self):
        if self.a <= 20:
            x = self.a
            self.a += 1
            return x
        else:
            raise StopIteration

myclass = MyNumbers()
myiter =iter(myclass)

print(next(myiter))
print(next(myiter))
print(next(myiter))
print(next(myiter))
print(next(myiter))
print(next(myiter))

for x in myiter:
    print(x)
