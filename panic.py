
def this_fails():
    x = 1/0

try:
    this_fails()
    print("after panic")
except ZeroDivisionError as err:
    print("handling run-time error:",err)
else:
    print("no error panic")

class MyError(Exception):
    def __init__(self,value):
        self.value = value
    def __str__(self):
        return repr(self.value)

try:
    raise MyError(2*2)
except MyError as e:
    print("my exception occurred, value:",e.value)