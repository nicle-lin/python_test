#!/usr/bin/python

class people:
    name = ''
    age = 0
    __weight = 0
    def __init__(self,n,a,w):
        self.name = n
        self.age = a
        self.__weight = w
    def speak(self):
        print("%s说：我%d岁,体重%d千克。"%(self.name,self.age,self.__weight))

p = people('baolin',10,30)
p.speak()
print(p.__init__("gubaolin",1,2))
p.speak()
print(p.name,p.age)

class student(people):
    grade = ''
    def __init__(self,n,a,w,g):
        people.__init__(self,n,a,w)
        self.grade = g
    def speak(self):
        print("%s说：我%d岁,我在读%d年级。"%(self.name,self.age,self.grade))

s = student('ken',10,60,3)
s.speak()

class speaker():
    topic = ''
    name = ''
    def __init__(self,n,t):
        self.name = n
        self.topic = t
    def speak(self):
        print("我叫 %s，我是一个演说家，我演讲的主题是 %s"%(self.name,self.topic))

class sample(speaker,student):
    a = ''
    def __init__(self,n,a,w,g,t):
        student.__init__(self,n,a,w,g)
        speaker.__init__(self,n,t)

    def __len__(self):
        print("this is length")

test = sample('tim',25,80,4,'python')
test.speak()
#print(len(test))

class Vector:
    def __init__(self,a,b):
        self.a = a
        self.b = b

    def __str__(self):
        return 'Vector(%d,%d)' %(self.a, self.b)

    def __add__(self, other):
        return Vector(self.a + other.a, self.b + other.b)


v1 = Vector(2,10)
v2 = Vector(5, -2)

print(v1+v2)
