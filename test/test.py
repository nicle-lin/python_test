# !/usr/bin/python

print("hello world!")
if False:
    print("this is true1")
    print("this is true2")
else:
 print("this is else1")
 print("this is else2")

str = 'RunoGab'
print(str)
print(str[0:-1])
print(str[:1])
print(str[:-2])
print(str[-2:-1])
print(str[-2:])
print("-----------------------")
print(str[0])
print(str[2:5])
print(str[0]*2)
print(str*2)
print(str*2+'你好')
print("----------------------------------")
print('hello\nworld\nI am good')
print(r'hello\n world\n I am good')

print("不换行输出", end=" ")
#print()
print("这本是第二行,不换行输出")

#input("\n\n please press enter key and exit")
# 注释
...
1
2
3
4
5

...